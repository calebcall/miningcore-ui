from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.poolindex, name='index'),
    url(r'^coin$', views.pooldata, name='pooldata'),
    url(r'^miners$', views.miners, name='miners'),
    url(r'^FAQ$', views.poolfaq, name='poolfaq'),
]