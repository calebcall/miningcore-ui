# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, render_to_response
from django.http import HttpResponse


import json
import urllib2
from itertools import izip

base_url = "http://api.minerscore.com"
api_port = "80"


def poolfaq(request):
    data = {'data': 'nothing'}
    return render(request, 'pool/faq.html', context=data)


def get_poolStats():
    data = json.loads(urllib2.urlopen("%s/api/pools" % (base_url)).read())
    return data


def get_blockStats(pool_id):
    block_data = json.loads(urllib2.urlopen("%s/api/pools/%s/blocks?pageSize=1000" % (base_url, pool_id)).read())
    data = {'blocks': block_data}
    return data


def get_minerStats(pool_id):
    miner_data = json.loads(urllib2.urlopen("%s/api/pools/%s/miners" % (base_url, pool_id)).read())
    miners = {'miners': miner_data}
    return miners


def get_poolPerformance(pool_id):
    perf_data = json.loads(urllib2.urlopen("%s/api/pools/%s/performance" % (base_url, pool_id)).read())
    data = {'performance': perf_data}
    return data


def get_poolPayments(pool_id):
    payment_data = json.loads(urllib2.urlopen("%s/api/pools/%s/payments" % (base_url, pool_id)).read())
    data = {'payments': payment_data}
    return data


def get_minerWallet(pool_id, wallet_id):
    miner_data = json.loads(urllib2.urlopen("%s/api/pools/%s/miners/%s" % (base_url, pool_id, wallet_id)).read())
    data = {'miner_wallet': miner_data}
    return data


def get_minerPayments(pool_id, wallet_id):
    miner_payment_data = json.loads(urllib2.urlopen("%s/api/pools/%s/miners/%s/payments" % (base_url, pool_id, wallet_id)).read())
    data = {'miner_payment': miner_payment_data}
    return data


def get_minerPerformance(pool_id, wallet_id):
    miner_performance_data = json.loads(urllib2.urlopen("%s/api/pools/%s/miners/%s/performance" % (base_url, pool_id, wallet_id)).read())
    data = {'miner_performance': miner_performance_data}
    return data


def poolindex(request):
    pool_data = get_poolStats()
    return render(request, 'pool/index.html', context=pool_data)


def pooldata(request):
    if request.GET:
        pool_id = request.GET.get('poolid', 'RVN')
    elif request.POST:
        pool_id = request.POST.get('poolid', 'RVN')
    else:
        pool_id = 'RVN'
    # pool_id = request.POST.get('poolid', "RVN")
    pool_data = get_poolStats()
    miners = get_minerStats(pool_id)
    performance_data = get_poolPerformance(pool_id)
    block_data = get_blockStats(pool_id)
    payment_data = get_poolPayments(pool_id)
    pool_info = {'poolid': pool_id}
    miner_data = dict(miners, **pool_info)
    mdata = dict(miner_data, **performance_data)
    pre_final_data = dict(mdata, **block_data)
    final_data = dict(pre_final_data, **pool_data)
    data1 = dict(payment_data, **final_data)
    json.dumps(pool_data)
    return render(request, 'pool/coin.html', context=data1)


def miners(request):
    if request.GET:
        wallet_id = request.GET.get('walletid', 'NONE')
        pool_id = request.GET.get('poolid', 'NONE')
    elif request.POST:
        wallet_id = request.POST.get('walletid', 'NONE')
        pool_id = request.POST.get('poolid', 'NONE')
    else:
        wallet_id = 'NONE'
        pool_id = 'NONE'
    miner_wallet = get_minerWallet(pool_id, wallet_id)
    performance = get_minerPerformance(pool_id, wallet_id)
    payments = get_minerPayments(pool_id, wallet_id)
    pool_info = {'poolid': pool_id, 'wallet': wallet_id}
    miner_stats = get_minerStats(pool_id)
    wallet_data = dict(miner_wallet, **payments)
    data = dict(wallet_data, **performance)
    data1 = dict(data, **pool_info)
    data2 = dict(data1, **miner_stats)
    return render(request, 'pool/miners.html', context=data2)
