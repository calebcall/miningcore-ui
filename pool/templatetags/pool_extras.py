from django import template
from datetime import datetime
from dateutil.parser import parse

register = template.Library()

@register.filter(name='multiply')
def multiply(value, arg):
    return value*arg

@register.filter(is_safe=True)
def division(value, arg):
    try:
        return float(value) / float(arg)
    except (ValueError, ZeroDivisionError):
        return None

@register.filter(is_safe=True)
def percentage(value):
    return value*100

@register.filter(is_safe=True)
def newdate(value):
    return parse(value)
