from django import template

register = template.Library()

@register.filter(is_safe=True)
def division(value, arg):
    try:
        return float(value) / float(arg)
    except (ValueError, ZeroDivisionError):
        return None